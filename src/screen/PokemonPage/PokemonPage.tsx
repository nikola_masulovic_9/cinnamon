import React, { useState } from "react";
import { useQuery } from "react-query";
import {
  fetchPokemonByName,
  fetchPokemonByType,
} from "../../services/pokemonService";
import Loader from "react-loader-spinner";
import styles from "./PokemonPage.module.css";
import Modal from "components/modal/Modal";

const PokemonPage: React.FC = () => {
  const [type, setType] = useState<number>(null);

  const url = window.location.pathname.split("/");

  const pokemon = useQuery([`pokemonByName_${url[2]}`], () =>
    fetchPokemonByName(url[2])
  );

  const pokemonByType = useQuery([`pokemonByType_${type}`], () =>
    fetchPokemonByType(type)
  );

  return (
    <div className={styles.homeDiv}>
      {pokemon.isSuccess && pokemon.data ? (
        <>
          <div className={styles.rowDiv}>
            <h2>Pokemon:</h2>
            <h2>{pokemon.data.name}</h2>
          </div>
          <div>
            <div className={styles.images}>
              {pokemon.data &&
                Object.keys(pokemon.data.sprites).map((spirit, index) => {
                  return (
                    <div key={index}>
                      {pokemon.data.sprites[spirit] !== null && index <= 7 && (
                        <img
                          src={pokemon.data.sprites[spirit]}
                          alt={pokemon.data.sprites[spirit]}
                        ></img>
                      )}
                    </div>
                  );
                })}
            </div>
            <div className={styles.rowDiv}>
              <h3>Pokemon types:</h3>
              {pokemon.data.types?.map((type,index) => {
                return (
                  <p
                    key={type.slot + index}
                    className={styles.pokemonType}
                    onClick={() => {
                      setType(type.slot);
                    }}
                  >
                    {type.type.name}
                  </p>
                );
              })}
            </div>
          </div>
        </>
      ) : (
        <>
          {pokemon.isLoading ? (
            <Loader
              type="Puff"
              color="#00BFFF"
              height={100}
              width={100}
            />
          ) : (
            <p>No pokemon found</p>
          )}
        </>
      )}
      {type && pokemonByType.data && (
        <Modal
          pokemons={pokemonByType.data}
          onClose={() => {
            setType(null);
          }}
        />
      )}
    </div>
  );
};

export default PokemonPage;
