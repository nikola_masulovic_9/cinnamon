import React, { useRef, useState } from "react";
import { useQuery } from "react-query";
import { fetchPokemons } from "../../services/pokemonService";
import { useHistory } from "react-router";
import styles from "./HomeScreen.module.css";
import { routePaths } from "config/routes";
import Loader from "react-loader-spinner";

const HomeScreen: React.FC = () => {
  const history = useHistory();
  const maxLimit = 1118
  const [limit, setLimit] = useState(100);
  const [search, setSearch] = useState('');
  const parentRef = useRef<HTMLDivElement>(null);

  const pokemons = useQuery(`pokemons_0-${limit}`, () =>
    fetchPokemons(limit, 0)
  );

  const onPokemonClick = (name: string) => {
    history.push({
      pathname: routePaths.POKEMON_PAGE.replace(":name", `${name}`),
    });
  };


  return (
    <div className={styles.homeDiv}>
      <img
        className={styles.pokemonTitle}
        alt="pokemonTitle"
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/1200px-International_Pok%C3%A9mon_logo.svg.png"
      ></img>
      <input
        value={search}
        onChange={(e) => {
          setSearch(e.target.value);
        }}
      ></input>
      <button onClick={() => onPokemonClick(search)}>Search</button>
      <button
        onClick={() => {
          setLimit(maxLimit);
        }}
      >
        View all
      </button>
      {pokemons.isSuccess && pokemons.data.results ? (
        <div ref={parentRef} className={styles.list}>
          {pokemons.data.results.map((pokemon) => (
            <div
              key={pokemon.name}
              style={{
                width: "100%",
                height: `50px`,
              }}
            >
              <p
                className={styles.pokemon}
                onClick={() => onPokemonClick(pokemon.name)}
              >
                {pokemon.name}
              </p>
            </div>
          ))}
        </div>
      ) : (
        <Loader
          type="Puff"
          color="#00BFFF"
          height={100}
          width={100}
        />
      )}
    </div>
  );
};

export default HomeScreen;
