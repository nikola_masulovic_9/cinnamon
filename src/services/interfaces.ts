export interface Pokemon {
  id: number;
  name: string;
  order: number;
  is_default: boolean;
  abilities: Ability[];
  types:Type[]
  weight:number
  sprites:Sprites
}

export interface Type {
    slot:number
    type:NameUrlInterface
}

export interface NameUrlInterface {
  name: string;
  url: string;
}

export interface Ability {
  ability: AbilityDesc;
  is_hidden: boolean;
  slot: number;
}

export interface AbilityDesc {
  name: string;
  url: string;
}

//ubaciti T
export interface PokemonResponse {
count:number
next:string
previous:string
results:NameUrlInterface[]
}

export interface PokemonResponse2 {
  count:number
  next:string
  previous:string
  results:Pokemon[]
}

export interface ResponseType {
  slot:number
  pokemon:NameUrlInterface
}

export interface PokemonResponseByType {
  pokemon:ResponseType[]
}

export interface Sprites {
  back_default:string
  back_female:string
  back_shiny:string
  back_shiny_female:string
  front_default:string
  front_female:string
  front_shiny:string
  front_shiny_female:string
}
