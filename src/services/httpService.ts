import axios from 'axios'

export const baseUrl = 'https://pokeapi.co/api/v2/'

export const http = axios.create({
  headers: {
    'X-Requested-With': 'XMLHttpRequest'
  }
})
