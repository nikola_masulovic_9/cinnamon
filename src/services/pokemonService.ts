import { baseUrl, http } from "./httpService"
import { Pokemon, PokemonResponse, PokemonResponseByType } from "./interfaces"


export const fetchPokemons = async (limit?:number, offset?:number) => {
 const response = await http.get<PokemonResponse>(baseUrl + 'pokemon',{
    params: { limit:limit ? limit : Number.MAX_SAFE_INTEGER, offset:offset ? offset : 0 }
  })
  return response.data
}

export const fetchPokemonByName = async (name:string) => {
  if(name){
    const response = await http.get<Pokemon>(baseUrl + `pokemon/${name}`)
    return response.data
  }
 return undefined
}

export const fetchPokemonByType = async (type:number) => {
  if(type){
  const response = await http.get<PokemonResponseByType>(baseUrl + `type/${type}`)
  return response.data
  }
  return undefined
}