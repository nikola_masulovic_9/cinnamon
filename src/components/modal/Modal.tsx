import React from "react";
import { PokemonResponseByType } from "services/interfaces";
import styles from "./Modal.module.css";

interface modalProps {
  onClose(): void;
  pokemons: PokemonResponseByType;
}

const Modal: React.FC<modalProps> = ({ onClose, pokemons }) => {
  return (
    <div className={styles.modal} id="modal">
      <div className={styles.actions}>
        <button
          className="toggle-button"
          onClick={() => {
            onClose();
          }}
        >
          close
        </button>
      </div>
      <div className={styles.list}>
        <p>By type:</p>
        {pokemons && (
          <div>
            {pokemons.pokemon.map((p) => {
              return <p key={p.pokemon.name}>{p.pokemon.name}</p>;
            })}
          </div>
        )}
      </div>
    </div>
  );
};

export default Modal;
