import { Switch, Route } from 'react-router'
import HomeScreen from 'screen/home/HomeScreen';
import {routePaths} from 'config/routes'
import PokemonPage from 'screen/PokemonPage/PokemonPage';
import './App.css';


function App() {
  return (
    <Switch>
      <Route exact path={routePaths.HOME} component={HomeScreen} />
      <Route
        path={routePaths.POKEMON_PAGE}
        component={PokemonPage}
      />
    </Switch>
  );
}

export default App;
